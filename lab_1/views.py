from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Salsabila Maurizka' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 4, 14) #TODO Implement this, format (Year, Month, Date)
npm = 1706043986 # TODO Implement this
hobi = 'Fencing'
tempat_kuliah = 'Universitas Indonesia'

kanan_name = 'Laila Saffanah'
kanan_date = date(1999,11,2)
npm1 = 1706043531
major = 'Computer Information System'
univ = 'Universitas Indonesia'
description = 'Animation concept enthusiast, mainly in steampunk concept'

kiri_name = 'Puspacinantya'
kiri_date = date(1998, 4, 8)
npm2 = 1706043821
hobi2 = 'sleep and eat'
desc2 = 'Semangat lincah gembira'

def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'hobi':hobi, 'tempat_kuliah':tempat_kuliah,
    'kanan_name': kanan_name, 'kanan_date': kanan_date, 'npm1': npm1, 'major': major, 'univ': univ, 'desc':description,
    'kiri_name': kiri_name, 'kiri_date': kiri_date, 'npm2': npm2, 'hobi2': hobi2, 'desc2':desc2}
    return render(request, 'storypertamaq.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
